#![no_std]

pub const fn hash(s: &str) -> usize {
    let mut result = 0_usize;
    let mut xs = s.as_bytes();
    while let Some((&v, vs)) = xs.split_first() {
        result = result.wrapping_mul(420);
        result = result.wrapping_add(v as usize);
        xs = vs;
    }
    result
}