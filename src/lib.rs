#![no_std]

use hash::hash;
pub use macros::mapped_literal_slice;

/// A statically-allocated hash-map.
pub struct StrMap<V: 'static> {
    array: [&'static [(&'static str, V)]],
}

impl<V> StrMap<V>
{
    pub fn get<'a>(&'a self, key: &str) -> Option<&'a V> {
        let i = hash(key) % self.array.len();
        let arr = *unsafe { self.array.get_unchecked(i) };
        arr.iter()
            .find(|(k, _)| *k == key)
            .map(|(_, v)| v)
    }
}

#[macro_export]
macro_rules! map {
    {type $t:ty; $($k:expr => $v:expr),*$(,)?} => {{
        let slice: &[&'static [(&'static str, $t)]] = $crate::mapped_literal_slice!($($k => $v),*);
        unsafe { &*(slice as *const [_] as *const $crate::StrMap<$t>) }
    }};
}