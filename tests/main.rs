use static_hashmap::map;

#[test]
fn general_test() {
    let shm = map! {
        type i32;
        "aaa" => 13,
        "i" => 69,
        "meaning" => 42,
    };

    assert_eq!(shm.get("meaning"), Some(&42));
    assert_eq!(shm.get("i"), Some(&69));
    assert_eq!(shm.get("aaa"), Some(&13));
    assert_eq!(shm.get("a"), None);
}
