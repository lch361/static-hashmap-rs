use proc_macro::{TokenStream, TokenTree, Literal, Punct, Spacing, Group, Delimiter};
use hash::hash;

struct Entry {
    key: String,
    value: TokenStream,
}

struct EntryIterator<I> {
    iter: I
}

impl<I> EntryIterator<I>
where
    I: Iterator<Item = TokenTree>
{
    fn new(iter: I) -> Self { Self { iter } }
}

fn get_key_literal(tree: TokenTree) -> String {
    match tree {
        TokenTree::Literal(v) => {
            let s = v.to_string();
            let c = s.chars().next().expect("Empty string literal");
            if c != '"' && c != '\'' {
                panic!("Non-str keys are not possible");
            }
            s[1..s.len() - 1].to_owned()
        },
        _ => panic!("Non-literal key encountered"),
    }
}

fn skip_arrow(iter: &mut impl Iterator<Item = TokenTree>) -> Result<(), ()> {
    match iter.next().ok_or(())? {
        TokenTree::Punct(v) => {
            if !(matches!(v.spacing(), Spacing::Joint) && v.as_char() == '=') {
                return Err(());
            }
        },
        _ => return Err(()),
    }
    match iter.next().ok_or(())? {
        TokenTree::Punct(v) => {
            if !(matches!(v.spacing(), Spacing::Alone) && v.as_char() == '>') {
                return Err(());
            }
        },
        _ => return Err(()),
    }
    Ok(())
}

impl<I> Iterator for EntryIterator<I>
where
    I: Iterator<Item = TokenTree>
{
    type Item = Entry;
    fn next(&mut self) -> Option<Self::Item> {
        let first = self.iter.next()?;
        let key = get_key_literal(first);
        if skip_arrow(&mut self.iter).is_err() {
            panic!("Key and value entries must be separated with =>");
        }
        let mut value = TokenStream::new();
        while let Some(v) = self.iter.next() {
            match v {
                TokenTree::Punct(v) => {
                    if matches!(v.spacing(), Spacing::Alone) && v.as_char() == ',' {
                        break;
                    }
                    value.extend([TokenTree::Punct(v)]);
                },
                _ => {
                    value.extend([v]);
                },
            }
        }
        Some(Entry { key, value })
    }
}

struct Map<'a> {
    mapped: Box<[Vec<&'a Entry>]>
}

impl<'a> Map<'a> {
    fn from_iter<I>(iter: I) -> Self
    where
        I: Iterator<Item = &'a Entry> + ExactSizeIterator
    {
        let size = iter.len() * 2;
        let mut buf = vec![vec![]; size].into_boxed_slice();
        for e in iter {
            let k = e.key.as_str();
            let i = hash(k) % size;
            let subvec = &mut buf[i];
            subvec.push(e);
        }
        Self { mapped: buf }
    }
}

fn reference() -> TokenStream {
    TokenStream::from_iter([
        TokenTree::Punct(Punct::new('&', proc_macro::Spacing::Alone)),
    ])
}

fn comma() -> TokenStream {
    TokenStream::from_iter([
        TokenTree::Punct(Punct::new(',', proc_macro::Spacing::Alone)),
    ])
}

fn array(contents: TokenStream) -> TokenStream {
    let result = TokenTree::Group(Group::new(Delimiter::Bracket, contents));
    TokenStream::from_iter([result])
}

fn pair_tuple(tuple: (TokenStream, TokenStream)) -> TokenStream {
    let mut result = TokenStream::new();
    result.extend(tuple.0);
    result.extend([TokenTree::Punct(Punct::new(',', proc_macro::Spacing::Alone))]);
    result.extend(tuple.1);
    let result = TokenTree::Group(Group::new(Delimiter::Parenthesis, result));
    TokenStream::from_iter([result])
}

impl Into<TokenStream> for Map<'_> {
    fn into(self) -> TokenStream {
        let mut result = TokenStream::new();
        result.extend(reference());
        let mut slice = TokenStream::new();

        for i in self.mapped.into_iter() {
            slice.extend(reference());
            let mut subslice = TokenStream::new();

            for &j in i {
                let key = Literal::string(&j.key);
                let key = TokenStream::from_iter([TokenTree::Literal(key)]);
                subslice.extend(pair_tuple((key, j.value.clone())));
                subslice.extend(comma());
            }
            
            slice.extend(array(subslice));
            slice.extend(comma());
        }

        result.extend(array(slice));
        result
    }
}

// Transform this:
// "a" => 4, "b" => 42
// Into this:
// &[&[("a", 4)], &[], &[("b", 42)], ...]
#[proc_macro]
pub fn mapped_literal_slice(mut tokens: TokenStream) -> TokenStream {
    // Apparently, transforming tokens => string => tokens
    // expands macro rep groups
    tokens = tokens.to_string().parse().expect("Parsing token stream somehow failed");
    let entries: Box<[Entry]> = EntryIterator::new(tokens.into_iter()).collect();
    let map = Map::from_iter(entries.into_iter());
    map.into()
}